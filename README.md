<img width=100% src="https://capsule-render.vercel.app/api?type=waving&color=00bfbf&height=120&section=header"/>

[![Typing SVG](https://readme-typing-svg.herokuapp.com/?color=00bfbf&size=50&center=true&vCenter=true&width=1000&lines=OLÁ,+MEU+NOME+É+PEDRO+MAURICIO;Sejá+Bem-vindo!+:%29)](https://git.io/typing-svg)

<div align = "left">
  <img width="49%" height="195px" alt="Pedro Mauricio github stats" src="https://github-readme-stats.vercel.app/api?username=pemauric&show_icons=true&theme=transparent&layout=compact&theme=transparent&hide_border=true&title_color=00bfbf&text_color=00bfbf&bg_color=0d1117&count_private=true"/>
  <img width="41%" height="195px" src="https://github-readme-stats.vercel.app/api/top-langs/?username=pemauric&layout=compact&theme=transparent&hide_border=true&title_color=00bfbf&text_color=00bfbf&bg_color=0d1117&count_private=true"/>
</div>

<h3 align="left">Tecnologias:</h3>
<div>
  <img src="https://github.com/devicons/devicon/blob/master/icons/javascript/javascript-original.svg" title="JavaScript" alt="JavaScript" width="40" height="40"/> &nbsp;
  <img src="https://github.com/devicons/devicon/blob/master/icons/nodejs/nodejs-original-wordmark.svg" title="NodeJs" alt="NodeJs" width="40" height="40"/> &nbsp;
  <img src="https://github.com/devicons/devicon/blob/master/icons/express/express-original.svg" title="ExpressJs" alt="ExpressJs" width="40" height="40"/> &nbsp;
  <img src="https://github.com/devicons/devicon/blob/master/icons/mysql/mysql-original-wordmark.svg" title="MySQL" alt="MySQL" width="40" height="40"/> &nbsp;
  <img src="https://github.com/devicons/devicon/blob/master/icons/mongodb/mongodb-original-wordmark.svg" title="MongoDB" alt="MongoDB" width="40" height="40"/> &nbsp;
  <img src="https://github.com/devicons/devicon/blob/master/icons/handlebars/handlebars-original-wordmark.svg" title="Handlebars" alt="Hanlebars" width="40" height="40"/> &nbsp;
  <img src="https://github.com/devicons/devicon/blob/master/icons/sequelize/sequelize-original.svg" title="Sequelize" alt="Sequelize" width="40" height="40"/> &nbsp;
</div>
<br>
<div>
  <h3>Veja alguns de meus projetos abaixo</h3>
  <a href = "https://pemauric.github.io/pong-p5.js/" target="_blank">Jogo Pong anos 70 - Feito com Javascript usando a biblioteca P5Js</a><br>
   <a href = "https://github.com/pemauric?tab=repositories" target="_blank">Clique aqui e veja mais em meus Repositórios!</a>
</div>
